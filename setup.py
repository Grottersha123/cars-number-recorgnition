
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages=[], excludes=[])
#
#                     include_files=['chars_responses.data', 'chars_samples.data', 'digits_responses.data', 'digits_samples.data', 'haarcascade_russian_plate_number.xml', 'Login_Sap.ui', 'Mode2.ui', 'Mode22.ui']
# )

import sys
#
# base = 'Win32GUI' if sys.platform == 'win32' else None

executables = [
    Executable('.py',)
]

setup(
    name='Licence'
    version='0.1',
    description='A PyQt licence Program',
    options=dict(build_exe=buildOptions),
    executables=executables
)